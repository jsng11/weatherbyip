# WeatherByIP

Webbasierte Systeme 2. Hausübung

Jonathan Schöngarth

## Service

WeatherByIP is a selfmade Mashup-App which uses the API [ip-geolocation](https://ip-geolocation.whoisxmlapi.com/api) and [openweathermap](https://openweathermap.org/api/) in order to provide weather information for a specific IP-address. The user needs to input and submit an IP-address which will be used as a query parameter for an api-request to ip-geolocation. The respond has information about the country, region and a city (best-case) and these information will be displayed on the result-page for the user. Besides that the city will be used as another query parameter this time for an api-request to openweathermap to get information about the main-weather, description, temperature and wind-speed shown on the result-page aswell.

This repository is deployed on heroku to see and use the functional aspects of the application : [https://sheltered-lowlands-24446.herokuapp.com/](https://sheltered-lowlands-24446.herokuapp.com/)

## Prerequisites

In order to clone this repository locally you need to install [git](https://git-scm.com/download/win).

Make sure you have [node.js](https://nodejs.org/en/download/) installed to run the code and to get npm (node package manager).


## Installation

First of all you need to clone the respository. In order to do that you need to go to the [Project-Overview](https://git.thm.de/jsng11/weatherbyip) and click on the clone - button. Choose clone with SSH and copy the shown link.

Before the following task make sure you have a own SSH-Key. If that is not the case go into your profile settings and read the instructions to SSH-Keys.

Now in order to clone the repository you need to write the following code into a terminal:

```bash
git clone git@git.thm.de:jsng11/weatherbyip.git
```
Once this is done you will have a new folder called weatherbyip. Move inside this folder with the cd command:

```bash
cd weatherbyip
```
You now need to install all dependencies from the project with your package manager:

```bash
npm install
```

Now there is one last task to do before running the application locally. You need to get 2 API-Keys to make api-requests with this app. Visit the APIs for [ip-geolocation](https://ip-geolocation.whoisxmlapi.com/api) and [openweathermap](https://openweathermap.org/api/) in order to create a account and a personal API-Key for each website.

Once you got both API-Keys you need to go into the project folder and go into the .env-File. Should look like this:

```bash
API_KEY_GEO={Add personal API-Key here}
API_KEY_WEATHER={Add personal API-Key here}
```
Replace the {Add personal API-Key here} with your own Key.

Once you have done all this the only thing left to do is running the project.

Use the terminal and go back into the project-folder. There you need to run the following command:

```bash
npm start
```

Now open a browser and type localhost:3000 into the url-field.



