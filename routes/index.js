var express = require('express');
var app = require('./../app');
var router = express.Router();


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
router.get('/:ipAddress', function(req, res, next) {
app.ipGeolocation(req.params.ipAddress, res);
});
router.post('/submitIP', function (req, res) {
  console.log("Submit worked");
  res.redirect('/'+req.body.ipAddress);
});
router.post('/back', function (req, res) {
  console.log("Back to Start");
  res.redirect('/');
});
module.exports = router;
