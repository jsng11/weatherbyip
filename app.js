var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const fetch = require("node-fetch");
require('dotenv').config();

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});





async function ipGeolocation(ipAddress, respond){
    const apiKey = process.env.API_KEY_GEO;
    const api = `https://ip-geolocation.whoisxmlapi.com/api/v1?apiKey=${apiKey}&ipAddress=${ipAddress}`;
    let json = await fetch(api)
        .then(function (res) {
            return res.json();
        });
    try{
        await openWeatherMap(
            ipAddress,
            json.location.country,
            json.location.region,
            json.location.city,
            respond
        );
    }catch (error){
        respond.render('inputError',{
            title: 'Something went wrong',
            ipAddress: ipAddress,
            errormsg: json.error
        });
    }
}
async function openWeatherMap(ipAddress,country,region,city, respond){
  const apiKey = process.env.API_KEY_WEATHER;
  const units = "metric";
  const lang = "en";
  const api = `http://api.openweathermap.org/data/2.5/weather?appid=${apiKey}&q=${city}&units=${units}&lang=${lang}`;
  let json = await fetch(api)
      .then(function (res) {
        return res.json();

      });
  try {
      respond.render('result', {
          title: 'Result',
          ipAddress: ipAddress,
          country: country,
          region: region,
          city: city,
          main: json.weather[0].main,
          description: json.weather[0].description,
          temp: json.main.temp,
          icon: json.weather[0].icon,
          windSpeed: json.wind.speed

      });
  }catch(error){
      respond.render('inputError',{
          title: 'Something went wrong',
          ipAddress: ipAddress,
          errormsg: json.message,
      });
  }
}
exports.ipGeolocation = ipGeolocation;
module.exports = app;
